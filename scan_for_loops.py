#!/usr/bin/env python3
from utils import init_local_libs
init_local_libs()
import os
import re
# Silence Git Python Warning
os.environ["GIT_PYTHON_REFRESH"] = "quiet"

import yaml

import tree_sitter
import tree_sitter.binding

import code_ast
from code_ast.visitor import ASTVisitor

from utils import main, pmap
from pretransforms import support_extensions

from tqdm import tqdm
from glob import glob

from typing import List


class LoopScanner(ASTVisitor):

    def __init__(self):
        self.has_loop = False
    
    def visit(self, node):
        return not self.has_loop

    # Loops ----------------------------------------------------------------

    def visit_for_statement(self, node):
        self.has_loop = True
        return False

    def visit_while_statement(self, node):
        self.has_loop = True
        return False

    def visit_do_statement(self, node):
        self.has_loop = True
        return False
    
def _parse_yml(filename):
    with open(filename, "r") as stream:
        try:
            content = yaml.safe_load(stream)
            properties = content["properties"]
            
            if not any("unreach-call" in prop["property_file"] for prop in properties):
                return None

            return os.path.join(os.path.dirname(filename), content["input_files"])
        except yaml.YAMLError as exc:
            print(exc)


def _parse_set_file(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()
    
    output = []

    base_dir = os.path.dirname(filename)

    for line in lines:
        if line.startswith("#"): continue
        line = line.strip()
        target = os.path.join(base_dir, line)
        output.extend(glob(target))

    return [_parse_yml(file_path) for file_path in output if os.path.isfile(file_path)]



def _list_files(input_files):
    output = []

    for file in input_files:
        output.extend(filter(lambda x: x is not None, _parse_set_file(file)))

    return output


def _has_loop(file_path):
    with open(file_path, 'r') as f:
        source = f.read()
    program_ast = code_ast.ast(source, lang = "c", syntax_error = "ignore")

    scanner = LoopScanner()
    program_ast.visit(scanner)

    return scanner.has_loop


def _try_has_loop(file_path):
    try:
        return _has_loop(file_path)
    except ValueError:
        return None


def scan_loops(
    input_files : List[str],
    ):

    total        =  0
    not_parsable = 0
    loop_count   = 0

    files = set(_list_files(input_files))
    pbar = tqdm(pmap(_try_has_loop, files), total = len(files))
    for result in pbar:
        total += 1
        if result is None: not_parsable += 1
        if result is True: loop_count += 1
        pbar.set_description("Num loops: %d, num no parse: %d" % (loop_count, not_parsable))

    print("Num files: ", total)
    print("Num files with loops: ", loop_count)
    print("Num not parsed: ", not_parsable)


# Main --------------------------

if __name__ == '__main__':
    main(scan_loops, version="0.1")