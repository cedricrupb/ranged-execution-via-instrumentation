import benchexec.tools.template
from benchexec import result

class Tool(benchexec.tools.template.BaseTool2):
    """
    This tool instruments a given program with a given range
    """

    def name(self):
        return "Range instrumenter for C programs"

    def executable(self, tool_locator):
        return tool_locator.find_executable("ranged_instrument.py")

    def version(self, executable):
        return self._version_from_tool(executable)

    def cmdline(self, executable, options, task, rlimits):
        return [executable] + options + [task.single_input_file]

    def determine_result(self, run):
        """
        @return: status of Instrumentor after executing a run
        """
        if run.exit_code.value != 0:
            return result.RESULT_ERROR
        else:
            return result.RESULT_DONE