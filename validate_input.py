#!/usr/bin/env python3
from utils import init_local_libs
init_local_libs()
import os
import re
# Silence Git Python Warning
os.environ["GIT_PYTHON_REFRESH"] = "quiet"


import tree_sitter
import tree_sitter.binding

import code_ast
from code_ast.visitor import ASTVisitor, ResumingVisitorComposition

import subprocess

from utils import main
from pretransforms import support_extensions, add_helper_functions
from instrument import AnalysisState, LocationAnalysis

from collections import Counter

FUNCTION_BLACKLIST = {}
FUNCTION_BLACKLIST_PATTERNS = []

def validate_input(
    program_file  : str, 
    bound         : str = "",
    loops_only    : bool = False,
    blacklist     : str  = "__VERIFIER_assert"
    ):

    setup_blacklist(blacklist)

    with open(program_file, "r") as f:
        source_code = f.read()
 
    # Run instrumentation
    if len(bound) == 0:
        print("[Dbg] No validation needed since no bound is given.")
    else:
        validate(source_code, bound, loops_only = loops_only)

    print("Done.")

# Validate input:

def _load_bound(bound_path):
    decisions = []
    line_ids  = []
    with open(bound_path, "r") as lines:
        for line in lines:
            line_id, decision = line.strip().split(",")
            decisions.append(decision)
            line_ids.append(int(line_id))
    return line_ids, decisions


def validate(source_code, bound_path, loops_only = False):
    program_ast = code_ast.ast(source_code, lang = "c", syntax_error = "warn")
    validate_ast(program_ast)

    line_ids,  _ = _load_bound(bound_path)
    validator = BranchProgramValidator(program_ast, line_ids, loops_only = loops_only)
    program_ast.visit(validator)


# Setup blacklist --------------------------------

def setup_blacklist(blacklist_string):
    FUNCTION_BLACKLIST = {}

    if len(blacklist_string) == 0: return
    if "," in blacklist_string:
        blacklist = [bstring.strip() for bstring in blacklist_string.split(",")]
    else:
        blacklist = [blacklist_string.strip()]

    for pattern in blacklist:
        try:
            FUNCTION_BLACKLIST_PATTERNS.append(re.compile(pattern))
        except re.error:
            FUNCTION_BLACKLIST.add(pattern)



FORBIDDEN_NAMES = {"branch_decision", "branch_decision_lower", "branch_decision_upper", "__RA_lower_branch_count", "__RA_upper_branch_count", "__RA_lower_path", "__RA_upper_path"}

def validate_ast(program_ast):
    
    class IdCollector(ASTVisitor):

        def __init__(self) -> None:
            super().__init__()
            self.identifiers = []

        def visit_identifier(self, node):
            self.identifiers.append(node)
    
    collector = IdCollector()
    program_ast.visit(collector)
    used_names = set(program_ast.match(node) for node in collector.identifiers)

    common_names = FORBIDDEN_NAMES & used_names
    if len(common_names) == 0: return

    raise ValueError(
        "The code contains identifiers that we need to generate the bounding logic: %s" % str(common_names)
    )


# Loop instrumentation ----------------------------------------------------------------

class BranchProgramValidator(ResumingVisitorComposition):
    
    def __init__(self, ast, line_ids, loops_only = False):
        self._state = AnalysisState(ast)
        self._state.line_ids = set(line_ids)

        self._location_analysis = LocationAnalysis(self._state)
        
        instrumentors = [BranchValidator(self._state, loops_only = loops_only)]

        super().__init__(self._location_analysis, *instrumentors)


def _error(node, message):
    error_message = f"{message} (at {node.type} [Line: {node.start_point[0]}])"
    raise ValueError(error_message)

def _name_node(function_node):
    declarator = function_node.child_by_field_name('declarator')
    if declarator is not None:
        name_node = declarator.child_by_field_name('declarator')
        if name_node is None or name_node.type != "identifier": return None
        return name_node
    return None


class BranchValidator(ASTVisitor):

    def __init__(self, state, loops_only = False):
        self._state = state
        self.ast   = state.ast
        self.loops_only = loops_only
        
        self._blocked_function = None
        self._blocked_name     = None

        self._safe_lines = set()

    # Visitor functions -----------------------------

    def _check_node(self, node, expect_id = False):
        start_line = node.start_point[0] + 1

        if start_line in self._safe_lines: return 

        if start_line in self._state.line_ids:
            if not expect_id:
                raise ValueError("Does not expect decision for %s in line %d but a decision was provided" % (node.type, start_line))
            if self._blocked_function is not None:
                raise ValueError("Does not expect decision for line %d in function %s (blacklisted) but a decision was provided" % (start_line, self._blocked_name))
            self._safe_lines.add(start_line)
        else:
            if expect_id and self._blocked_function is None:
                raise ValueError("Expect decision for line %d but none found" % start_line)


    def visit_function_definition(self, node):
        function_name_node = _name_node(node)
        if function_name_node is None: return True
        function_name = self.ast.match(function_name_node)
        if function_name in FUNCTION_BLACKLIST: 
            self._blocked_function = node
            self._blocked_name = function_name

        if any(pattern.match(function_name) for pattern in FUNCTION_BLACKLIST_PATTERNS):
            self._blocked_function = node
            self._blocked_name = function_name
    
    def leave_function_definition(self, node):
        if self._blocked_function == node: self._blocked_function = None

    # Statement visitors -----------------------------

    def visit_expression_statement(self, node):
        self._check_node(node, False)

    def visit_if_statement(self, node):
        if self.loops_only: 
            self._check_node(node, False)
            return True
        self._check_node(node, True)

    # Loops ----------------------------------------------------------------

    def enter_loop(self, node):
        self._check_node(node, True)

    def visit_for_statement(self, node):
        self.enter_loop(node)


    def visit_while_statement(self, node):
        self.enter_loop(node)

    
    def visit_do_statement(self, node):
        self.enter_loop(node)

    # Ignore statements -------------------------------------

    def visit_switch_statement(self, node):
        _error(node, "Switch statement are not supported")

    def visit_compound_statement(self, node):
        return True

    def visit(self, node):
        if node.type.endswith("statement"):
            self._check_node(node, False)




# Main --------------------------

if __name__ == '__main__':
    main(validate_input, version="0.1")