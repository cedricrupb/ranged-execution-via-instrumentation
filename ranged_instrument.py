#!/usr/bin/env python3
from utils import init_local_libs
init_local_libs()
import os
import re
# Silence Git Python Warning
os.environ["GIT_PYTHON_REFRESH"] = "quiet"


import tree_sitter
import tree_sitter.binding

import code_ast
from code_ast.visitor import ASTVisitor, ResumingVisitorComposition

import subprocess

from utils import main
from pretransforms import support_extensions, add_helper_functions
from instrument import AnalysisState, LocationAnalysis

from collections import Counter

FUNCTION_BLACKLIST = {}
FUNCTION_BLACKLIST_PATTERNS = []

def ranged_instrument(
    program_file       : str, 
    output_file        : str = "instrumented.c",
    lower              : str = "",
    upper              : str = "",
    valid_check        : bool = False,
    inline             : bool = False,
    reverse            : bool = False,
    loops_only         : bool = False,
    blacklist          : str  = "__VERIFIER_assert",
    validate_input     : bool = False,
    reduced_instrument : bool = False,
    ):

    setup_blacklist(blacklist)

    if validate_input or reduced_instrument:
        print("[WARNING] The instrumentor expects that the bounds are annotated with a location.")

    if validate_input:
        check_input(program_file, lower, loops_only = loops_only, blacklist = blacklist)
        check_input(program_file, upper, loops_only = loops_only, blacklist = blacklist)

    with open(program_file, "r") as f:
        source_code = f.read()
 
    # Run instrumentation
    if len(lower) == 0 and len(upper) == 0:
        transformed_code = source_code # No instrumentation needed
    else:
        if inline:
            if reverse: raise ValueError("Reverse mode is currently unmaintained.")
            loop_instrument = BranchInstrumentationInlineFn(lower, upper, reverse_mode = reverse, loops_only = loops_only, path_only = reduced_instrument)
        else:
            if reverse: raise ValueError("Reverse mode is only supported together with inline mode.")
            loop_instrument = BranchInstrumentationFn(lower, upper, loops_only = loops_only, path_only = reduced_instrument)
        transformed_code = support_extensions(source_code, loop_instrument)

    with open(output_file, "w") as o:
        o.write(transformed_code)
    
    if valid_check: check_validity(output_file)

    print("Done.")

# Setup blacklist --------------------------------

def setup_blacklist(blacklist_string):
    FUNCTION_BLACKLIST = {}

    if len(blacklist_string) == 0: return

    if "," in blacklist_string:
        blacklist = [bstring.strip() for bstring in blacklist_string.split(",")]
    else:
        blacklist = [blacklist_string.strip()]

    for pattern in blacklist:
        try:
            FUNCTION_BLACKLIST_PATTERNS.append(re.compile(pattern))
        except re.error:
            FUNCTION_BLACKLIST.add(pattern)
    

# Validation ----------------------------------------------------

def check_input(program_file, bound_path, loops_only = False, blacklist = ""):
    if len(bound_path) == 0: return

    from validate_input import validate_input
    print("Validate input: %s" % bound_path)
    validate_input(program_file, bound_path, loops_only, blacklist)
    print("Run instrumentor:")


def check_validity(output_file):
    output_file = os.path.abspath(output_file)

    try:
        # Run clang with the -fsyntax-only option and capture the return code
        subprocess.run(['clang', '-fsyntax-only', output_file], capture_output=True, check=True)
        
        # Print a success message if the syntax check passed
        print('Syntax check passed.')
    
    except FileNotFoundError:
        # Raise an exception indicating that clang is not installed
        raise Exception('clang is not installed on this machine')
        
    except subprocess.CalledProcessError as e:
        # Raise an exception with the error message from clang
        error_message = e.stderr.decode().strip()
        raise Exception(f'clang failed with the following error:\n{error_message}')



# Lower Templates ------------------------------------------------

LOWER_FALSE_TEMPLATE = """
    if({cond}){{
        assume_abort_if_not0(!cond);
    }} else {{
        if(!cond){path_var} = 0;
    }}
"""

LOWER_TRUE_TEMPLATE = """
    if({cond}){{
        if(!cond){path_var} = 0;
    }} else {{
        assume_abort_if_not0(!cond);
    }}
"""

LOWER_TEMPLATES = {
    "true": LOWER_TRUE_TEMPLATE,
    "false": LOWER_FALSE_TEMPLATE
}

LOWER_LOGIC_TEMPLATE = """

unsigned int {path_var}  = 1;
unsigned int {count_var} = 0;
void {func_name}(int cond){{
    if(!{path_var})return;
    {inner_logic}
    {count_var}++;
    if({count_var} >= {max_var}){path_var} = 0;

}}


"""



# ----------------------------------------------------------

def _load_bound(path):
    decisions = []
    with open(path, "r") as lines:
        for line in lines:
            _, decision = line.strip().split(",")
            decisions.append(decision)
    return decisions


def _load_path(path_str):
    if len(path_str) == 0 or not os.path.exists(path_str): return []
    path = []
    with open(path_str, "r") as lines:
        for line in lines:
            line_idx, _ = line.strip().split(",")
            path.append(int(line_idx))
    return path


def _compress_bound(bound):
    counter = Counter(bound)

    if "false" not in counter: counter["false"] = 0
    if "true" not in counter : counter["true"] = 0

    least_common, _ = counter.most_common()[-1]
    return [i for i, l in enumerate(bound) if l == least_common], least_common


# Lower ---------------------------------------

def _add_lower_function(func_name, lower_path, count_var = "__RA_lower_branch_count", path_var = "__RA_lower_path"):
    bound = _load_bound(lower_path)
    max_var = len(bound)
    decision_cond, decision = _compress_bound(bound)

    if len(decision_cond) == 0:
        cond = "0"
    else:
        cond = " || ".join(f"{count_var} == {c}" for c in decision_cond)

    inner_logic = LOWER_TEMPLATES[decision].format(cond = cond, path_var = path_var)
    return LOWER_LOGIC_TEMPLATE.format(
        func_name   = func_name,
        inner_logic = inner_logic,
        count_var   = count_var,
        path_var    = path_var,
        max_var     = max_var
    )


# Upper Templates ------------------------------------------------

UPPER_FALSE_TEMPLATE = """
    if({cond}){{
        if(cond){path_var} = 0;
    }} else {{
        assume_abort_if_not0(cond);
    }}
"""

UPPER_TRUE_TEMPLATE = """
    if({cond}){{
        assume_abort_if_not0(cond);
    }} else {{
        if(cond){path_var} = 0;
    }}
"""

UPPER_TEMPLATES = {
    "true": UPPER_TRUE_TEMPLATE,
    "false": UPPER_FALSE_TEMPLATE
}

UPPER_LOGIC_TEMPLATE = """

unsigned int {path_var}  = 1;
unsigned int {count_var} = 0;
void {func_name}(int cond){{
    if(!{path_var})return;
    if({count_var} >= {max_var}){{
        assume_abort_if_not0(cond);
        return;
    }}
    {inner_logic}
    {count_var}++;
}}


"""

# We know that the lower bound also explores the only true path
# Therefore, we can save time here and abort early.
UPPER_LOGIC_OPTIM_TEMPLATE = """

unsigned int {path_var}  = 1;
unsigned int {count_var} = 0;
void {func_name}(int cond){{
    if(!{path_var})return;
    if({count_var} >= {max_var}){{
        abort();
    }}
    {inner_logic}
    {count_var}++;
}}


"""

EMPTY_PROGRAM = """

extern void abort(void);
void reach_error() {}
void __VERIFIER_assert(int cond) { if (!(cond)) { ERROR: { reach_error(); abort(); } } }

int main() {
    return 0;
}

"""



# Upper ---------------------------------------

def _add_upper_function(func_name, upper_path, count_var = "__RA_upper_branch_count", path_var = "__RA_upper_path"):
    bound = _load_bound(upper_path)
    max_var = len(bound)
    decision_cond, decision = _compress_bound(bound)

    if len(decision_cond) == 0:
        cond = "0"
    else:
        cond = " || ".join(f"{count_var} == {c}" for c in decision_cond)

    inner_logic = UPPER_TEMPLATES[decision].format(cond = cond, path_var = path_var)
    return UPPER_LOGIC_OPTIM_TEMPLATE.format(
        func_name   = func_name,
        inner_logic = inner_logic,
        count_var   = count_var,
        path_var    = path_var,
        max_var     = max_var
    )


# Instrumentor ---------------------------

FORBIDDEN_NAMES = {"branch_decision", "branch_decision_lower", "branch_decision_upper", "__RA_lower_branch_count", "__RA_upper_branch_count", "__RA_lower_path", "__RA_upper_path"}

def validate_ast(program_ast):
    
    class IdCollector(ASTVisitor):

        def __init__(self) -> None:
            super().__init__()
            self.identifiers = []

        def visit_identifier(self, node):
            self.identifiers.append(node)
    
    collector = IdCollector()
    program_ast.visit(collector)
    used_names = set(program_ast.match(node) for node in collector.identifiers)

    common_names = FORBIDDEN_NAMES & used_names
    if len(common_names) == 0: return

    raise ValueError(
        "The code contains identifiers that we need to generate the bounding logic: %s" % str(common_names)
    )


def validate_lower(lower_path, reverse = False):
    bound = _load_bound(lower_path)
    decision_cond, decision = _compress_bound(bound)
    target = "true" if reverse else "false"
    return len(decision_cond) > 0 or decision != target

def validate_upper(upper_path, reverse = False):
    bound = _load_bound(upper_path)
    decision_cond, decision = _compress_bound(bound)
    target = "true" if reverse else "false"
    return len(decision_cond) > 0 or decision != target


class BranchInstrumentationFn:

    def __init__(self, lower, upper, branch_fn = "branch_decision", loops_only = False, path_only = False):
        self.lower = lower
        self.upper = upper
        self.branch_fn = branch_fn
        self.loops_only = loops_only
        self.path_only = path_only
    
    def __call__(self, source_code):
        program_ast = code_ast.ast(source_code, lang = "c", syntax_error = "warn")

        validate_ast(program_ast)

        prefix       = []
        branch_calls = []

        if len(self.lower) > 0 and validate_lower(self.lower):
            func_name = self.branch_fn + "_lower"
            prefix.append(_add_lower_function(func_name, self.lower))
            branch_calls.append(func_name)
        
        if len(self.upper) > 0:
            if not validate_upper(self.upper):
                print("[OPTIM] The execution is empty. Return empty program.")
                return EMPTY_PROGRAM

            func_name = self.branch_fn + "_upper"
            prefix.append(_add_upper_function(func_name, self.upper))
            branch_calls.append(func_name)
        
        if len(branch_calls) == 0: 
            print("[OPTIM] Instrumentation not necessary. Return original program.")
            return source_code # No instrumentation needed
        
        # Reduce instrumentation
        instrumentation_whitelist = None
        if self.path_only:
            instrumentation_whitelist = set.union(set(_load_path(self.lower)), set(_load_path(self.upper)))

        # Run instrumentation
        instrumenter = BranchProgramInstrumentor(program_ast, branch_fn = self.branch_fn, loops_only = self.loops_only, instrumentation_whitelist = instrumentation_whitelist)
        program_ast.visit(instrumenter)

        code = instrumenter.code()

        # Add function definition
        branch_calls = "".join([f"    %s(cond);\n" % call for call in branch_calls])
        prefix.append(
"""
void %s(int cond){
%s
}
""" % (self.branch_fn, branch_calls)
        )
        code = "".join(prefix + [code])
        code = add_helper_functions(code, ["assume_abort_if_not0"], ast = program_ast)
        return code

# Inline support ----------------------------------------------------------------

# int lread(){return {count_var} >= {max_var} || {cond};}

# Lower true
# if({path_var}){
#   path_var = !lread();
#   {count_var}++;
# }

# Lower false
# if({path_var}){
#   if(lread()) abort();
#   {count_var}++;
# }


# Upper true
# if({path_var}){
#   if(!lread())abort();
#   {count_var}++;
# }

# Upper false
# if({path_var}){
#   {path_var} = lread();
#   {count_var}++;
# }




# True
TRUE_UPPER_TEMPLATE = """
if({path_var}){{
    if({count_var} >= {max_var}) abort();
    {path_var} = !({cond});
    {count_var}++;
}} 

"""

TRUE_UPPER_TRIVIAL_TRUE_TEMPLATE = """
if({path_var}){{
    if({count_var} >= {max_var}) abort();
    {path_var} = 0;
}} 
"""

TRUE_UPPER_TRIVIAL_FALSE_TEMPLATE = """
if({path_var}){{
    if({count_var} >= {max_var}) abort();
    {count_var}++;
}} 
"""

TRUE_LOWER_TEMPLATE = """
if({path_var}){{
    {path_var} = {count_var} < {max_var};
    if({path_var} & ({cond})) abort();
    {count_var}++;
}}
"""


TRUE_LOWER_TRIVIAL_TRUE_TEMPLATE = """
if({path_var}) abort();
"""

TRUE_LOWER_TRIVIAL_FALSE_TEMPLATE = """
if({path_var}) {{
    {path_var} = {count_var} < {max_var};
    {count_var}++;
}}
"""

# False
FALSE_UPPER_TEMPLATE = """
if({path_var}){{
    if({count_var} >= {max_var}) abort();
    if({cond}) abort();
    {count_var}++;
}}
"""

FALSE_UPPER_TRIVIAL_TRUE_TEMPLATE = """
if({path_var}) abort();
"""

FALSE_UPPER_TRIVIAL_FALSE_TEMPLATE = """
if({path_var}){{
    if({count_var} >= {max_var}) abort();
    {count_var}++;
}}
"""

FALSE_LOWER_TEMPLATE = """
if({path_var}){{
    {path_var} = {count_var} < {max_var};
    {path_var} = {path_var} & !({cond});
    {count_var}++;
}}

"""

FALSE_LOWER_TRIVIAL_TRUE_TEMPLATE = """
{path_var} = 0;
"""

FALSE_LOWER_TRIVIAL_FALSE_TEMPLATE = """
if({path_var}){{
    {path_var} = {count_var} < {max_var};
    {count_var}++;
}}
"""


INLINE_TEMPLATES = {
    ("true", "lower", "1") : TRUE_LOWER_TRIVIAL_TRUE_TEMPLATE,
    ("true", "lower", "0") : TRUE_LOWER_TRIVIAL_FALSE_TEMPLATE,
    ("true", "lower")      : TRUE_LOWER_TEMPLATE,
    ("true", "upper", "1") : TRUE_UPPER_TRIVIAL_TRUE_TEMPLATE,
    ("true", "upper", "0") : TRUE_UPPER_TRIVIAL_FALSE_TEMPLATE,
    ("true", "upper")      : TRUE_UPPER_TEMPLATE,
    ("false", "lower", "1"): FALSE_LOWER_TRIVIAL_TRUE_TEMPLATE,
    ("false", "lower", "0"): FALSE_LOWER_TRIVIAL_FALSE_TEMPLATE,
    ("false", "lower")     : FALSE_LOWER_TEMPLATE,
    ("false", "upper", "1"): FALSE_UPPER_TRIVIAL_TRUE_TEMPLATE,
    ("false", "upper", "0"): FALSE_UPPER_TRIVIAL_FALSE_TEMPLATE,
    ("false", "upper")     : FALSE_UPPER_TEMPLATE,
}



# Reverse Inline support ----------------------------------------------------------------

# True
REV_TRUE_LOWER_TEMPLATE = """
if({path_var}){{
    {path_var} = !({cond});
    {count_var}++;
    {path_var} = {path_var} && ({count_var} < {max_var});
}}
"""

REV_TRUE_LOWER_TRIVIAL_TRUE_TEMPLATE = """{path_var} = 0;"""

REV_TRUE_LOWER_TRIVIAL_FALSE_TEMPLATE = """{path_var} = {path_var} && ((++{count_var}) < {max_var});"""

REV_TRUE_UPPER_TEMPLATE = """
if({path_var} && ({count_var} >= {max_var} || ({cond}))) abort();
{count_var}++;
"""

REV_TRUE_UPPER_TRIVIAL_TRUE_TEMPLATE = """
if({path_var}) abort();
"""

REV_TRUE_UPPER_TRIVIAL_FALSE_TEMPLATE = """
if({path_var} && ({count_var}++) >= {max_var}) abort();
"""

# False 
REV_FALSE_LOWER_TEMPLATE = """
if({path_var} && ({cond})) abort();
{path_var} = {path_var} && ((++{count_var}) < {max_var});
"""

REV_FALSE_LOWER_TRIVIAL_TRUE_TEMPLATE = """
if({path_var}) abort();
"""

REV_FALSE_LOWER_TRIVIAL_FALSE_TEMPLATE = """{path_var} = {path_var} && ((++{count_var}) < {max_var});"""

REV_FALSE_UPPER_TEMPLATE = """
if({path_var} && {count_var} >= {max_var}) abort();
{path_var} = {path_var} && !({cond});
{count_var}++;
"""

REV_FALSE_UPPER_TRIVIAL_TRUE_TEMPLATE = """
if({path_var} && ({count_var}++) >= {max_var}) abort();
{path_var} = 0;
"""

REV_FALSE_UPPER_TRIVIAL_FALSE_TEMPLATE = """
if({path_var} && ({count_var}++) >= {max_var}) abort();
"""


REV_INLINE_TEMPLATES = {
    ("true", "lower", "1") : REV_TRUE_LOWER_TRIVIAL_TRUE_TEMPLATE,
    ("true", "lower", "0") : REV_TRUE_LOWER_TRIVIAL_FALSE_TEMPLATE,
    ("true", "lower")      : REV_TRUE_LOWER_TEMPLATE,
    ("true", "upper", "1") : REV_TRUE_UPPER_TRIVIAL_TRUE_TEMPLATE,
    ("true", "upper", "0") : REV_TRUE_UPPER_TRIVIAL_FALSE_TEMPLATE,
    ("true", "upper")      : REV_TRUE_UPPER_TEMPLATE,
    ("false", "lower", "1"): REV_FALSE_LOWER_TRIVIAL_TRUE_TEMPLATE,
    ("false", "lower", "0"): REV_FALSE_LOWER_TRIVIAL_FALSE_TEMPLATE,
    ("false", "lower")     : REV_FALSE_LOWER_TEMPLATE,
    ("false", "upper", "1"): REV_FALSE_UPPER_TRIVIAL_TRUE_TEMPLATE,
    ("false", "upper", "0"): REV_FALSE_UPPER_TRIVIAL_FALSE_TEMPLATE,
    ("false", "upper")     : REV_FALSE_UPPER_TEMPLATE,
}


def _positive_cond(count_var, decision_cond):
    if len(decision_cond) == 0:
        return "0"
    else:
        return " || ".join(f"{count_var} == {c}" for c in decision_cond)
    
def _negative_cond(count_var, decision_cond):
    #if len(decision_cond) == 0:
    #    return "1"
    #else:
    #    return " && ".join(f"{count_var} != {c}" for c in decision_cond)
    return f"!({_positive_cond(count_var, decision_cond)})"


class BranchInstrumentationInlineFn:

    def __init__(self, lower, upper, branch_fn = "branch_decision", reverse_mode = False, loops_only = False, path_only = False):
        self.lower = lower
        self.upper = upper
        self.branch_fn = branch_fn
        self.reverse_mode = reverse_mode
        self.loops_only   = loops_only
        self.path_only    = path_only

        self._globals = set()

    def _build_decision(self, target, bound_type, decision_cond, count_var, negate = False, **kwargs):
        cond = _positive_cond(count_var, decision_cond) if not negate else _negative_cond(count_var, decision_cond)

        if self.reverse_mode:
            inline_templates = REV_INLINE_TEMPLATES
        else:
            inline_templates = INLINE_TEMPLATES

        if cond in ["1", "0"]:
            template = inline_templates[(target, bound_type, cond)]
            return template.format(count_var = count_var, **kwargs)
        else:
            template = inline_templates[(target, bound_type)]
            return template.format(cond = cond, count_var = count_var, **kwargs)


    def _build_instrument(self, target, lower_count_var = "__RA_lower_branch_count", lower_path_var = "__RA_lower_path", upper_count_var = "__RA_upper_branch_count", upper_path_var = "__RA_upper_path"):
        if len(self.lower) > 0:
            bound = _load_bound(self.lower)
            lower_max_var = len(bound)
            lower_decision_cond, lower_decision = _compress_bound(bound)
            self._globals.add(lower_count_var)
            self._globals.add(lower_path_var)
        else:
            lower_decision = "unknown"
        
        if len(self.upper) > 0:
            bound = _load_bound(self.upper)
            upper_max_var = len(bound)
            upper_decision_cond, upper_decision = _compress_bound(bound)
            self._globals.add(upper_count_var)
            self._globals.add(upper_path_var)
        else:
            upper_decision = "unknown"

        if upper_decision == "unknown" and lower_decision == "unknown": return ""

        output = []

        if lower_decision != "unknown":
            output.append(
                self._build_decision(
                    target, "lower", lower_decision_cond, lower_count_var,
                    path_var = lower_path_var, max_var = lower_max_var,
                    negate = lower_decision == target,
                )
            )
        
        if upper_decision != "unknown":
            output.append(
                self._build_decision(
                    target, "upper",  upper_decision_cond, upper_count_var,
                    path_var = upper_path_var, max_var = upper_max_var, 
                    negate = upper_decision == target,
                )
            )

        result = "\n".join(output)

        # Hack: Replace double negations
        result = result.replace("!(!", "(")

        return result

    def _build_true_instrumentation(self, lower_count_var = "__RA_lower_branch_count", lower_path_var = "__RA_lower_path", upper_count_var = "__RA_upper_branch_count", upper_path_var = "__RA_upper_path"):
        return self._build_instrument("true", lower_count_var, lower_path_var, upper_count_var, upper_path_var)


    def _build_false_instrumentation(self, lower_count_var = "__RA_lower_branch_count", lower_path_var = "__RA_lower_path", upper_count_var = "__RA_upper_branch_count", upper_path_var = "__RA_upper_path"):
        return self._build_instrument("false", lower_count_var, lower_path_var, upper_count_var, upper_path_var)

    def _build_globals(self):
        output = []

        for var in sorted(self._globals, key = len):
            if "path" in var:
                output.append(f"unsigned int {var} = 1;")
            else:
                output.append(f"unsigned int {var} = 0;")

        return "\n".join(output) + "\n"

    def __call__(self, source_code):
        num_instrumented = 0
        if len(self.lower) > 0 and validate_lower(self.lower, self.reverse_mode):
            num_instrumented += 1
        
        if len(self.upper) > 0:
            if not validate_upper(self.upper, self.reverse_mode):
                print("[OPTIM] The execution is empty. Return empty program.")
                return EMPTY_PROGRAM
            num_instrumented += 1
        
        if num_instrumented == 0:
            print("[OPTIM] Instrumentation not necessary. Return original program.")
            return source_code # No instrumentation needed
        
        program_ast = code_ast.ast(source_code, lang = "c", syntax_error = "warn")

        validate_ast(program_ast)

        # Instrumentation phase -----------------------------------------------------

        # Reduce instrumentation
        instrumentation_whitelist = None
        if self.path_only:
            instrumentation_whitelist = set.union(set(_load_path(self.lower)), set(_load_path(self.upper)))

        # Run instrumentation
        instrumenter = BranchProgramInstrumentor(program_ast, branch_fn = self.branch_fn, loops_only = self.loops_only, instrumentation_whitelist = instrumentation_whitelist)
        program_ast.visit(instrumenter)
        code = instrumenter.code()

        cond_true_code, cond_false_code = self._build_true_instrumentation(), self._build_false_instrumentation()
        code = code.replace(f"{self.branch_fn}(1);", cond_true_code).replace(f"{self.branch_fn}(0);", cond_false_code)
        code = self._build_globals() + code

        # Add function definition
        code = add_helper_functions(code, ["assume_abort_if_not0"], ast = program_ast)
        return code



# Loop instrumentation ----------------------------------------------------------------

class BranchProgramInstrumentor(ResumingVisitorComposition):
    
    def __init__(self, ast, branch_fn = "branch_decision", loops_only = False, instrumentation_whitelist = None):
        self._state = AnalysisState(ast)
        self._state.instrumentation = []

        self._location_analysis = LocationAnalysis(self._state)
        
        instrumentors = [BranchInstrumentor(self._state, branch_fn = branch_fn, loops_only = loops_only, instrumentation_whitelist = instrumentation_whitelist)]

        super().__init__(self._location_analysis, *instrumentors)

    def code(self):
        
        source_lines    = self._state.ast.source_lines
        instrumentation = sorted(self._state.instrumentation, key = lambda x: x[0])
        if len(instrumentation) == 0: return "\n".join(source_lines)

        output = []

        instrumentation.append(((len(source_lines), 0), ""))

        current_pos = [0, 0]
        for pos, annotation in instrumentation:
            if tuple(current_pos) < pos:
                while current_pos[0] < pos[0]:
                    output.append(source_lines[current_pos[0]][current_pos[1]:] + "\n")
                    current_pos[0] += 1
                    current_pos[1] = 0

                if current_pos[1] < pos[1]:
                    output.append(source_lines[current_pos[0]][current_pos[1]:pos[1]])
                    current_pos[1] = pos[1]
            
            output.append(annotation)

            # TODO: Currently hacky / Fix
            if annotation.endswith("\n") and current_pos[1] > 0: output.append(" "*current_pos[1])

        return "".join(output)


def _error(node, message):
    error_message = f"{message} (at {node.type} [Line: {node.start_point[0]}])"
    raise ValueError(error_message)

def _name_node(function_node):
    declarator = function_node.child_by_field_name('declarator')
    if declarator is not None:
        name_node = declarator.child_by_field_name('declarator')
        if name_node is None or name_node.type != "identifier": return None
        return name_node
    return None


class BranchInstrumentor(ASTVisitor):

    def __init__(self, state, branch_fn = "branch_decision", loops_only = False, instrumentation_whitelist = None):
        self._state = state
        self.ast   = state.ast
        self.branch_fn = branch_fn
        self.loops_only = loops_only
        self.instrumentation_whitelist = instrumentation_whitelist


    def accept_instrumentation(self, line):
        if self.instrumentation_whitelist is None: return True
        print(line + 1, self.instrumentation_whitelist)
        return (1 + line) in self.instrumentation_whitelist

 
    def instrument(self, text, pos = None):
        pos = pos or (self._state.line, self._state.line_offset)

        self._state.instrumentation.append(
            (pos, text)
        )

    def branch(self, condition, pos = None):
        branch = f"{self.branch_fn}({condition});\n"
        self.instrument(branch, pos = pos)

    # Visitor functions -----------------------------

    def visit_function_definition(self, node):
        function_name_node = _name_node(node)
        if function_name_node is None: return True
        function_name = self.ast.match(function_name_node)
        if function_name in FUNCTION_BLACKLIST: return False

        return not any(pattern.match(function_name) for pattern in FUNCTION_BLACKLIST_PATTERNS)

    # Statement visitors -----------------------------

    def visit_expression_statement(self, node):
        return True

    def visit_if_statement(self, node):
        if self.loops_only: return True
        if not self.accept_instrumentation(node.start_point[0]): return True

        # Ensure every statement in compound
        consequence = node.child_by_field_name("consequence")
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("{\n", pos = consequence.start_point)
            else:
                consequence = consequence.children[1]

            self.branch("1", pos = consequence.start_point)

        alternative = node.child_by_field_name("alternative") 
        if alternative is not None: 
            if alternative.type != "compound_statement":
                self.instrument("{\n", pos = alternative.start_point)
            else:
                alternative = alternative.children[1]
            self.branch("0", pos = alternative.start_point)
    

    def leave_if_statement(self, node):
        if self.loops_only: return True
        if not self.accept_instrumentation(node.start_point[0]): return True

        # Ensure every statement in compound
        consequence = node.child_by_field_name("consequence")
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("\n}", pos = consequence.end_point)
        
        alternative = node.child_by_field_name("alternative") 
        if alternative is not None: 
            if alternative.type != "compound_statement":
                self.instrument("\n}", pos = alternative.end_point)
        else:
            consequence = node.child_by_field_name("consequence")
            self.instrument(" else {\n", pos = consequence.end_point)
            self.branch("0", pos = consequence.end_point)
            self.instrument("}\n", pos = consequence.end_point)

    # Loops ----------------------------------------------------------------

    def enter_loop(self, node, consequence, pos = None):
        if not self.accept_instrumentation(node.start_point[0]): return
        if pos is None: pos = consequence.start_point
        self.branch("1", pos = pos)

    def leave_loop(self, node, consequence, pos = None):
        if not self.accept_instrumentation(node.start_point[0]): return
        if pos is None: pos = consequence.end_point
        self.branch("0", pos = pos)
    
    def visit_for_statement(self, node):
        consequence = node.children[-1]
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("{\n", pos = consequence.start_point)
            else:
                consequence = consequence.children[1]

        self.enter_loop(node, consequence)


    def leave_for_statement(self, node):
        # Ensure every statement in compound
        consequence = node.children[-1]
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("\n}", pos = consequence.end_point)

        self.leave_loop(node, consequence)

    def visit_while_statement(self, node):
        consequence = node.children[-1]
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("{\n", pos = consequence.start_point)
            else:
                consequence = consequence.children[1]

        self.enter_loop(node, consequence)

    def leave_while_statement(self, node):
        # Ensure every statement in compound
        consequence = node.children[-1]
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("\n}", pos = consequence.end_point)
        
        self.leave_loop(node, consequence)
    
    def visit_do_statement(self, node):
        consequence = node.child_by_field_name("body")
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("{\n", pos = consequence.start_point)
            else:
                consequence = consequence.children[1]
        
        self.enter_loop(node, consequence)

    def leave_do_statement(self, node):
        # Ensure every statement in compound
        consequence = node.child_by_field_name("body")
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("\n}", pos = consequence.end_point)
        
        self.leave_loop(node, consequence, pos = node.end_point)

    # Ignore statements -------------------------------------

    def visit_attributed_statement(self, node):
        return True

    def visit_labeled_statement(self, node):
        return True

    def visit_compound_statement(self, node):
        return True

    def visit_return_statement(self, node):
        return True

    def visit_switch_statement(self, node):
        _error(node, "Switch statement are not supported")

    def visit_goto_statement(self, node):
        return True

    def visit_continue_statement(self, node):
        return True
    
    def visit_break_statement(self, node):
        return True

    def visit(self, node):
        if node.type.endswith("statement"):
            print("[Dbg] No instrumentation for %s" % node.type)




# Main --------------------------

if __name__ == '__main__':
    main(ranged_instrument, version="0.1")