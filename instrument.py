from utils import main
from pretransforms import support_extensions

import code_ast
from code_ast.visitor import ASTVisitor, ResumingVisitorComposition


def instrument(program_file : str, output_file : str):
    
    with open(program_file, "r") as f:
        source_code = f.read()

    # Run instrumentation
    transformed_code = support_extensions(source_code, _instrument)

    with open(output_file, "w") as o:
        o.write("#include <stdio.h>\n")
        o.write(transformed_code)


def _instrument(source_code):
    program_ast = code_ast.ast(source_code, lang = "c", syntax_error = "warn")

    # Run instrumentation
    instrumenter = ProgramInstrumenter(program_ast)
    program_ast.visit(instrumenter)

    return instrumenter.code()

# Instrumentation ----------------------------------------------------------------

class AnalysisState:
    def __init__(self, ast):
        self.ast = ast
    
    def __getattr__(self, name):
        return self.__dict__.get(name, None)


class ProgramInstrumenter(ResumingVisitorComposition):
    
    def __init__(self, ast):
        self._state = AnalysisState(ast)
        self._state.instrumentation = []

        self._location_analysis = LocationAnalysis(self._state)
        self._instrumenter      = Instrumenter(self._state)

        super().__init__(self._location_analysis, self._instrumenter)

    def code(self):
        
        source_lines    = self._state.ast.source_lines
        instrumentation = sorted(self._state.instrumentation, key = lambda x: x[0])
        if len(instrumentation) == 0: return "\n".join(source_lines)

        output = []

        instrumentation.append(((len(source_lines), 0), ""))

        current_pos = [0, 0]
        for pos, annotation in instrumentation:
            if tuple(current_pos) < pos:
                while current_pos[0] < pos[0]:
                    output.append(source_lines[current_pos[0]][current_pos[1]:] + "\n")
                    current_pos[0] += 1
                    current_pos[1] = 0

                if current_pos[1] < pos[1]:
                    output.append(source_lines[current_pos[0]][current_pos[1]:pos[1]])
                    current_pos[1] = pos[1]
            
            output.append(annotation)

        return "".join(output)


class LocationAnalysis(ASTVisitor):

    def __init__(self, state):
        self._state = state

        self._state.line        = 0
        self._state.line_offset = 0

    def on_visit(self, node):
        start_point = node.start_point
        assert (self._state.line, self._state.line_offset) <= start_point
        self._state.line = start_point[0]
        self._state.line_offset = start_point[1]
        return super().on_visit(node)
    
    def on_leave(self, node):
        end_point = node.end_point
        self._state.line = end_point[0]
        self._state.line_offset = end_point[1]
        return super().on_leave(node)


class Instrumenter(ASTVisitor):

    def __init__(self, state):
        self._state = state
        self.ast    = state.ast

        self.scope   = []
        self.control = None


    def instrument(self, text, pos = None):
        pos = pos or (self._state.line, self._state.line_offset)

        self._state.instrumentation.append(
            (pos, text)
        )



if __name__ == '__main__':
    main(instrument)