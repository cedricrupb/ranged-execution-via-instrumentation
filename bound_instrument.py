#!/usr/bin/env python3
from utils import init_local_libs
init_local_libs()
import os
# Silence Git Python Warning
os.environ["GIT_PYTHON_REFRESH"] = "quiet"


import tree_sitter
import tree_sitter.binding

import tree_sitter
import tree_sitter.binding

import code_ast
from code_ast.visitor import ASTVisitor, ResumingVisitorComposition

from utils import main
from pretransforms import support_extensions, add_helper_functions
from instrument import AnalysisState, LocationAnalysis

SKIP_FUNCS = ["reach_error", "__VERIFIER_error", "abort", "assume_abort_if_not"]

def loop_instrument(
    program_file : str, 
    output_file  : str = "instrumented.c",
    lower        : int = -1,
    upper        : int = -1
    ):

    with open(program_file, "r") as f:
        source_code = f.read()

    # Run instrumentation
    if lower == -1 and upper == -1:
        transformed_code = source_code # No instrumentation needed
    else:
        if lower != -1 and upper != -1:
            assert lower <= upper, f"Invalid interval: [{lower} - {upper}]"

        loop_instrument = LoopInstrumentationFn(lower, upper)
        transformed_code = support_extensions(source_code, loop_instrument)

    with open(output_file, "w") as o:
        o.write(transformed_code)

    print("Done.")


class LoopInstrumentationFn:

    def __init__(self, lower, upper):
        self.lower = lower
        self.upper = upper
    
    def __call__(self, source_code):
        program_ast = code_ast.ast(source_code, lang = "c", syntax_error = "warn")

        # Run instrumentation
        instrumenter = LoopProgramInstrumentor(program_ast, self.lower, self.upper)
        program_ast.visit(instrumenter)

        code = instrumenter.code()
        code = add_helper_functions(code, ["assume_abort_if_not"], ast = program_ast)
        return code


# Loop instrumentation ----------------------------------------------------------------
class LoopProgramInstrumentor(ResumingVisitorComposition):
    
    def __init__(self, ast, lower, upper):
        self._state = AnalysisState(ast)
        self._state.instrumentation = []

        self._location_analysis = LocationAnalysis(self._state)
        
        instrumentors = []

        if lower > 0:
            self._lower_instrumentor = LowerLoopInstrumentor(self._state, lower)
            instrumentors.append(self._lower_instrumentor)
        
        if upper > 0:
            self._upper_instrumentor = UpperLoopInstrumentor(self._state, upper)
            instrumentors.append(self._upper_instrumentor)


        super().__init__(self._location_analysis, *instrumentors)

    def code(self):
        
        source_lines    = self._state.ast.source_lines
        instrumentation = sorted(self._state.instrumentation, key = lambda x: x[0])
        if len(instrumentation) == 0: return "\n".join(source_lines)

        output = []

        instrumentation.append(((len(source_lines), 0), ""))

        current_pos = [0, 0]
        for pos, annotation in instrumentation:
            if tuple(current_pos) < pos:
                while current_pos[0] < pos[0]:
                    output.append(source_lines[current_pos[0]][current_pos[1]:] + "\n")
                    current_pos[0] += 1
                    current_pos[1] = 0

                if current_pos[1] < pos[1]:
                    output.append(source_lines[current_pos[0]][current_pos[1]:pos[1]])
                    current_pos[1] = pos[1]
            
            output.append(annotation)

            # TODO: Currently hacky / Fix
            if annotation.endswith("\n") and current_pos[1] > 0: output.append(" "*current_pos[1])

        return "".join(output)


def _error(node, message):
    error_message = f"{message} (at {node.type} [Line: {node.start_point[0]}])"
    raise ValueError(error_message)

class BaseLoopInstrumentor(ASTVisitor):

    def __init__(self, state):
        self._state = state
        self.ast   = state.ast

        self._state.loop_counter_registry = {}

        self.scope = []
        
        self.loop_count = 0
        self.loop_scope = []

    def instrument(self, text, pos = None):
        pos = pos or (self._state.line, self._state.line_offset)

        self._state.instrumentation.append(
            (pos, text)
        )

    def assume(self, condition, pos = None):
        assume = f"assume_abort_if_not({condition});\n"
        self.instrument(assume, pos = pos)

    # Visitor functions -----------------------------

    def visit_function_definition(self, node):

       # Decide scope
       declarator = node.child_by_field_name("declarator")
       
       while declarator.type != "function_declarator":
            declarator = declarator.child_by_field_name("declarator")

       func_name  = declarator.child_by_field_name("declarator")
       assert func_name.type == "identifier", func_name.type

       func_name = self.ast.match(func_name)
       self.scope.append(func_name)

       if func_name in SKIP_FUNCS: return False 
    

    def leave_function_definition(self, node):
        self.scope.pop(-1)

    # Statement visitors -----------------------------

    def visit_expression_statement(self, node):
        return True

    def visit_if_statement(self, node):
        # Ensure every statement in compound
        consequence = node.child_by_field_name("consequence")
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("{\n", pos = consequence.start_point)
            else:
                consequence = consequence.children[1]

        alternative = node.child_by_field_name("alternative") 
        if alternative is not None: 
            if alternative.type != "compound_statement":
                self.instrument("{\n", pos = alternative.start_point)
            else:
                alternative = alternative.children[1]
    

    def leave_if_statement(self, node):
        # Ensure every statement in compound
        consequence = node.child_by_field_name("consequence")
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("\n}", pos = consequence.end_point)
        
        alternative = node.child_by_field_name("alternative") 
        if alternative is not None: 
            if alternative.type != "compound_statement":
                self.instrument("\n}", pos = alternative.end_point)

    # Loops ----------------------------------------------------------------

    def _node_key(self, node):
        return (node.type, node.start_point, node.end_point)

    def _loop_counter_identifier(self, id):
        return "__LOOP_COUNTER_%d" % id


    def _create_loop_counter(self, node):
        node_key = self._node_key(node)
        loop_counter_registry = self._state.loop_counter_registry

        if node_key in loop_counter_registry:
            return self._loop_counter_identifier(loop_counter_registry[node_key])

        loop_counter_id = len(loop_counter_registry)
        loop_counter_registry[node_key] = loop_counter_id
        loop_counter_identifier = self._loop_counter_identifier(loop_counter_id)

        loop_counter_def = f"unsigned int {loop_counter_identifier} = 0;\n"
        self.instrument(loop_counter_def, pos = node.start_point)

        incr_statement = f"{loop_counter_identifier}++;"

        consequence = node.children[-1]
        if consequence.type == "compound_statement":
            last_statement = consequence.children[-2]
        else:
            last_statement = consequence.children[-1]
        
        self.instrument("\n" + incr_statement, pos=last_statement.end_point)

        return loop_counter_identifier

    def _enter_loop(self, node, consequence):
        pass

    def _leave_loop(self, node, consequence):
        pass

    def _check_loop_validity(self, node):
        if len(self.loop_scope) > 1:
            _error(node, "No support for nested loops")
        if self.loop_count > 1:
            _error(node, "No support for multiple loops")

    def enter_loop(self, node, consequence):
        self._create_loop_counter(node)
        self.loop_scope.append(node)
        self.loop_count += 1
        self._check_loop_validity(node)
        self._enter_loop(node, consequence)

    def leave_loop(self, node, consequence):
        assert self.loop_scope.pop(-1) == node
        self._leave_loop(node, consequence)
    
    def visit_for_statement(self, node):
        consequence = node.children[-1]
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("{\n", pos = consequence.start_point)
            else:
                consequence = consequence.children[1]

        self.enter_loop(node, consequence)


    def leave_for_statement(self, node):
        # Ensure every statement in compound
        consequence = node.children[-1]
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("\n}", pos = consequence.end_point)

        self.leave_loop(node, consequence)

    def visit_while_statement(self, node):
        consequence = node.children[-1]
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("{\n", pos = consequence.start_point)
            else:
                consequence = consequence.children[1]

        self.enter_loop(node, consequence)

    def leave_while_statement(self, node):
        # Ensure every statement in compound
        consequence = node.children[-1]
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("\n}", pos = consequence.end_point)
        
        self.leave_loop(node, consequence)
    
    def visit_do_statement(self, node):
        consequence = node.child_by_field_name("body")
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("{\n", pos = consequence.start_point)
            else:
                consequence = consequence.children[1]
        
        self.enter_loop(node, consequence)

    def leave_do_statement(self, node):
        # Ensure every statement in compound
        consequence = node.child_by_field_name("body")
        if consequence is not None: 
            if consequence.type != "compound_statement":
                self.instrument("\n}", pos = consequence.end_point)
        
        self.leave_loop(node, consequence)

    # Ignore statements -------------------------------------

    def visit_attributed_statement(self, node):
        return True

    def visit_labeled_statement(self, node):
        return True

    def visit_compound_statement(self, node):
        return True

    def visit_return_statement(self, node):
        return True

    def visit_switch_statement(self, node):
        # Ignore for now. Should we adress this?
        pass

    # We can safely ignore control statements
    def visit_goto_statement(self, node):
        _error(node, "No support for goto statements")
        return True

    def visit_continue_statement(self, node):
        _error(node, "No support for continue statements")
        return True
    
    def visit_break_statement(self, node):
        _error(node, "No support for break statement")
        return True

    def visit(self, node):
        if node.type.endswith("statement"):
            print("[Dbg] No instrumentation for %s" % node.type)


class LowerLoopInstrumentor(BaseLoopInstrumentor):
    """Lower bound on loop executions"""

    def __init__(self, state, k):
        super().__init__(state)
        self.k = k

    def _leave_loop(self, node, consequence):
        loop_counter = self._create_loop_counter(node)
        condition = f"{loop_counter} >= {self.k}"
        self.assume(condition, pos = node.end_point)


class UpperLoopInstrumentor(BaseLoopInstrumentor):
    """Upper bound on loop executions"""
    
    def __init__(self, state, k):
        super().__init__(state)
        self.k = k

    def _enter_loop(self, node, consequence):
        loop_counter = self._create_loop_counter(node)

        if consequence.type == "compound_statement":
            first_statement = consequence.children[1]
        else:
            first_statement = consequence.children[0]
        
        condition = f"{loop_counter} < {self.k}"
        self.assume(condition, pos = first_statement.start_point)


# Main --------------------------

if __name__ == '__main__':
    main(loop_instrument, version="0.1")