import benchexec.tools.template


class Tool(benchexec.tools.template.BaseTool2):
    """
    This tool instruments a given program with a loop bound
    """

    def name(self):
        return "Loop bound instrumenter for C programs"

    def executable(self, tool_locator):
        return tool_locator.find_executable("bound_instrument.py")

    def version(self, executable):
        return self._version_from_tool(executable)

    def cmdline(self, executable, options, task, rlimits):
        return [executable] + options + [task.single_input_file]