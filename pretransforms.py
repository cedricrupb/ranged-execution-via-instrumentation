import re
from typing import List

import code_ast

def remove_comments(text):
    """
    removes comments from a piece of source code.
    source: https://stackoverflow.com/questions/241327/remove-c-and-c-comments-using-python
    """
    def replacer(match):
        s = match.group(0)
        if s.startswith('/'):
            return " "  # note: a space and not an empty string
        else:
            return s
    pattern = re.compile(
        r'//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*"',
        re.DOTALL | re.MULTILINE
    )
    return re.sub(pattern, replacer, text)


def regex(code: str) -> str:
    r"""
    creates a regex expression matching the code with changed whitespace
    does not support string and char values:
    "" would become "\s*", the same for ''
    """
    # shrink whitespace
    code = re.sub(r"\s+", " ", code)
    code = re.sub(r"(?! )(\W)(?! )(\W)", r"\1 \2", code)
    code = re.sub(r"(\w)(?! )(\W)", r"\1 \2", code)
    code = re.sub(r"(?! )(\W)(\w)", r"\1 \2", code)
    code = re.sub(r"(\w) (\w)", r"\1\n\n\2", code)

    # escape special symbols
    code = re.sub(r"(?=[\\(){}\[\].+$^*?|])", r"\\", code)

    # replace whitespace
    code = re.sub(r" ", r"\\s*", code)
    code = re.sub(r"\n\n", r"\\s+", code)

    return code


def unsupported_to_extern(code: str, replacings: List, unsupported: str) -> str:
    """replaces any method containing the unsupported string with an extern method"""
    while r := re.search(unsupported, code):
        start = open = code.find("{")
        depth = 1
        close = code.find("}")
        while True:
            if close < open:
                end = close
                close = code.find("}", close) + 1
                depth -= 1
                if depth == 0:
                    if start < r.start() < close:
                        break
                    start = open
                    depth = 1
            else:
                open = code.find("{", open + 1)
                if open == -1: raise ValueError("Something went wrong")
                depth += 1
        previous_end = max(code[:start].rfind(";"), code[:start].rfind("}")) + 1
        save = code[previous_end:end]
        code = ";".join([code[:start], code[end:]])
        code = "extern ".join([code[:previous_end], code[previous_end:]])
        replacings += [(regex(code[previous_end:start + 8]), save)]
    
    return code


def support_extensions(code: str, func):
    """remove code which can not be parsed by pycparser, do func and reconstruct incompatible code afterwards"""
    #code = remove_comments(code)
    replacings = []  # pattern-string combinations which later must be replaced

    # remove unsupported parts
    try:
        code = unsupported_to_extern(code, replacings, "__extension__")
    except ValueError:
        replacings = []

    for keyword in ("inline", "restrict"):
        if f"__{keyword} " in code and not re.search(f"(?<!__){keyword}", code):
            code = code.replace(f"__{keyword}", keyword)
            replacings += [(keyword, f"__{keyword}")]

    any = "[a-zA-Z0-9()_*, \n]*"
    attr_or_const = r"(__attribute__ *\(\([a-zA-Z0-9_, ]*\)\)|__const )"
    while r := re.search(f"extern{any}{attr_or_const}{any};", code):
        replacings += [(regex(re.sub(attr_or_const, "", r.group())), r.group())]
        code = re.sub(f"extern{any}{attr_or_const}{any};", re.sub(attr_or_const, " ", r.group()), code, 1)

    # execute func
    code = func(code)

    # reconstruct incompatible code
    for pair in replacings:
        code = re.sub(pair[0], pair[1], code)
    return code

# Add helper functions ----------------------------------------------------------------

ASSUME = """
extern void abort(void);
void assume_abort_if_not(int cond) {
  if(!cond) {abort();}
}
"""

ASSUME0 = """
extern void abort(void);
void assume_abort_if_not0(int cond) {
  if(!cond) {abort();}
}
"""


HELPER_FNS = {
    "assume_abort_if_not": ASSUME,
    "assume_abort_if_not0": ASSUME0,
}

class FunctionDeclarationVisitor(code_ast.ASTVisitor):
    def __init__(self):
        self.fns = []

    def visit_function_definition(self, node):
        return self.visit(node.child_by_field_name("declarator"))
    
    def visit_function_declarator(self, node):
        name_node = node.child_by_field_name("declarator")
        if name_node is None or name_node.type != "identifier": return False
        self.fns.append(name_node)
        return False


def _track_fn_declarations(code, ast = None):
    if ast is None:
        ast = code_ast.ast(code, lang = "c", syntax_error = "warn")
    visitor = FunctionDeclarationVisitor()
    ast.visit(visitor)

    return [ast.match(fn_node) for fn_node in visitor.fns]


def add_helper_functions(code, fns, ast = None):
    assert all(fn in HELPER_FNS for fn in fns), "Cannot create all functions: %s" % str(fns)

    all_declared_fns = set(_track_fn_declarations(code, ast = ast))
    
    code_prefix = []
    for fn in fns:
        if fn not in all_declared_fns:
            code_prefix.append(HELPER_FNS[fn])
    
    code_prefix.append(code)
    return "".join(code_prefix)
